/**programming III lab3 junit test
 * class Vector3dTests by using junit to test Vector3d class
 * Author: Shirley Id: 2037242
 * version: 2021-09-13
 */

package LinerAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class Vector3dTests {
    // this test will fail
    @Test
    public void test() {
        fail("this test will fail");
    }
    //confirm the get methods
    @Test
    public void testGet(){
        Vector3d vector7=new Vector3d(-1,3.0,4.0);
        assertEquals(-1.0, vector7.getX());
        assertEquals(3.0, vector7.getY());
        assertEquals(4.0, vector7.getZ());
    }

    //confirm magnitude methods
    @Test
    public void testMagnitude(){
        Vector3d vector1=new Vector3d(2.0,3.0,4.0);
        assertEquals(5.385164807134504,vector1.magnitude() );
    }

    @Test
    //confirms that the dotProduct() method returns the correct result.
    public void testDotProduct() {
        Vector3d vector5=new Vector3d(2.0,3.0,4.0);
        Vector3d vector6=new Vector3d(1.0,2.0,3.0);
        assertEquals(20,vector6.dotProduct(vector5) );
    }
    //confirm add()
    @Test
    public void testAdd(){
        Vector3d vector1=new Vector3d(2.0,3.0,4.0);
        Vector3d vector2=new Vector3d(1.0,2.0,3.0);
        Vector3d newVector=vector2.add(vector1);
        assertEquals(3.0, newVector.getX());
        assertEquals(5.0, newVector.getY());
        assertEquals(7.0, newVector.getZ());
    }
}
