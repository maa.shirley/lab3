package LinerAlgebra;

public class VectorApplication {
    public static void main(String[] args){
      
        Vector3d vector1=new Vector3d(1,3,4);
        Vector3d vector2=new Vector3d(1,2,3);
        Vector3d vector3=new Vector3d(-1,2,3);
        System.out.println(vector3.getX());
        System.out.println(vector2.getY());
       
        System.out.println(vector2.dotProduct(vector1));
        System.out.println(vector2.add(vector1));
        System.out.println(vector1.magnitude());
    }
    
}
