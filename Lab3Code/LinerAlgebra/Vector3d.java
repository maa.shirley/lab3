/**programming III lab3
 * package LinerAlebrs class Vector3d
 * The class Vector3d should have 3 fields: x, y, and z. 
 * All 3 fields should be doubles and the class will be an immutable class 
 * author: Student ID:2037242
 * version: 09-11-2021
 */
package LinerAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;
 
    /*contructor
    *2	You should add a constructor that takes as 
    *input 3 doubles and sets the 3 fields accordingly
    */
    public Vector3d(double x, double y, double z){
        
        this.x=x;
        this.y=y;
        this.z=z;
    }
    //3. get method for 3 fields
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    } 
    public double getZ(){
        return this.z;
    }
    /* 4a.A method magnitude() which returns the 
    magnitude of the this Vector3d. The magnitude can
     be calculated using the formula
    */   
    public double magnitude(){
        double magnitude;
        magnitude=Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2)+Math.pow(this.z, 2));
        return magnitude;
    } 
    /*4b.	A method dotProduct() which should take as input another 
    Vector3d and return a double representing the dot product of the
    two vectors. The dot product is calculated by multiplying each 
    corresponding term and then adding the results together. 
    For example:(1,1,2) dot (2,3,4) = (1 * 2) + (1 * 3) + (2 * 4) = 13
    */
    public double dotProduct(Vector3d Vector){
        double calculateDot;
        calculateDot=this.x * Vector.getX() + this.y * Vector.getY() + this.z *Vector.getZ();
        return calculateDot;
    }
    /*4c.	A method add() which should take as input another 
    Vector3d and return a new Vector3d calculated by adding 
    the this Vector3d with the other input. This can be done
    by adding each term individually. For example: (1,1,2) + (2,3,4) = (3,4,6)
    */
    public Vector3d add(Vector3d vector){
        Vector3d newVector=new Vector3d(this.x+vector.getX(), this.y+vector.getY(), this.z+vector.getZ());
        return newVector;
        
    }
    //toString method
    public String toString(){
        return "X :"+getX()+ ", Y :"+getY()+ ", Z :" +getZ();
    }


}
